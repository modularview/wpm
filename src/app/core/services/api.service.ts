import {Injectable} from '@angular/core';
import {Dictionary} from "../types";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // mockup data (instead of getting the API from the server).
  fetchDictionary(): Dictionary {
    return {
      'en_US': [
        'many solutions were suggested for this problem',
        'earth on which he lived was a microscopic dot',
        'he drove on being wrong about all these things',
        'landscape gardening it is the future',
        'the winning team will be the first team that wins',
      ],
      'he_IL': [
        'בערבות הנגב מסתובב גמל',
        'שעוד לא אכל לחם עם בצל',
        'מסתובב גמל שעוד לא אכל',
        'בערבות הנגב לחם עם בצל',
        'מסתובב גמל מסתובב גמל',
      ],
    };
  }
}
