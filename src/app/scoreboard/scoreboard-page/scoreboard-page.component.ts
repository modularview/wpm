import { Component, OnInit } from '@angular/core';
import {StorageService} from "../../core/services/storage.service";

@Component({
  selector: 'app-scoreboard-page',
  templateUrl: './scoreboard-page.component.html',
  styleUrls: ['./scoreboard-page.component.scss']
})
export class ScoreboardPageComponent implements OnInit {

  classResult: string;
  typingTime: string;
  backspaceCounter: number;

  constructor(private storageService: StorageService) { }

  ngOnInit(): void {
    this.classResult = this.storageService.getClassResult() ? 'Successfully' : 'with errors';
    this.typingTime = this.storageService.getTime();
    this.backspaceCounter = this.storageService.getBackspaceCounter();
  }

}
