import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {StartPageComponent} from "./start/start-page/start-page.component";
import {TypingPageComponent} from "./typing/typing-page/typing-page.component";
import {ScoreboardPageComponent} from "./scoreboard/scoreboard-page/scoreboard-page.component";

const routes: Routes = [
  {
    path: '',
    component: StartPageComponent
  },
  {
    path: 'typing',
    component: TypingPageComponent
  },
  {
    path: 'scoreboard',
    component: ScoreboardPageComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
