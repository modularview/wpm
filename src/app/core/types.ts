export enum languages {
  en_US = 'en_US',
  he_IL = 'he_IL',
}

export type Dictionary = {
  [key in languages]: Array<string>;
}
