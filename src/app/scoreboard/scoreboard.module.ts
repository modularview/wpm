import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScoreboardPageComponent } from './scoreboard-page/scoreboard-page.component';
import {AppRoutingModule} from "../app-routing.module";



@NgModule({
  declarations: [ScoreboardPageComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
  ]
})
export class ScoreboardModule { }
