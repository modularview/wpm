import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartPageComponent } from './start-page/start-page.component';
import {AppRoutingModule} from "../app-routing.module";
import {FormsModule} from "@angular/forms";



@NgModule({
  declarations: [StartPageComponent],
  imports: [
    CommonModule,
    FormsModule,
    AppRoutingModule
  ]
})
export class StartModule { }
